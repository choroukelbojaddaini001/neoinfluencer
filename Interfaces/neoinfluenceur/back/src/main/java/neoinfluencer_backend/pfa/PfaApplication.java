package neoinfluencer_backend.pfa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@EntityScan("neoinfluencer_backend.pfa.model")
@EnableConfigurationProperties

public class PfaApplication {
	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(PfaApplication.class, args);
		System.out.println("Hello me");


	}
}