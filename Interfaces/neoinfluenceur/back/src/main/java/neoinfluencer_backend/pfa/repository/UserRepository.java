package neoinfluencer_backend.pfa.repository;

import neoinfluencer_backend.pfa.model.User;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	Optional<User> findOneByEmailAndPassword(String email, String encodedPassword);

    User findByEmail(String email);

    List<User> findAllByEmail(String email);

    boolean existsByEmail(String email);

}
