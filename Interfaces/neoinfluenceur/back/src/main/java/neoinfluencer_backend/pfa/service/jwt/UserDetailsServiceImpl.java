package neoinfluencer_backend.pfa.service.jwt;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import neoinfluencer_backend.pfa.model.User;
import neoinfluencer_backend.pfa.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService  {
    
       
       @Autowired
    private UserRepository userRepository;
      @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        List<User> users = userRepository.findAllByEmail(email);

        if (!users.isEmpty()) {
            for (User user : users) {
                String encodedPassword = user.getPassword();
                return new org.springframework.security.core.userdetails.User(
                        user.getEmail(),
                        encodedPassword,
                        Collections.singletonList(new SimpleGrantedAuthority("USER"))
                );
            }
        }

        throw new UsernameNotFoundException("User not found with email: " + email);
    }
}
