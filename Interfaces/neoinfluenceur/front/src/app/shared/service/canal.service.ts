import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CanalService {

  constructor(private http : HttpClient) { }
  cards = [
    {
      name:'Facebook',
      imageUrl: 'assets/AssetsCanauxImages/fb.png',
     
    },
    {
      name: 'Youtube',
      imageUrl: 'assets/AssetsCanauxImages/youtube.svg'
    },
    {
      name: 'Instagram',
      imageUrl: 'assets/AssetsCanauxImages/insta.png'
    },
   
    {
      name: 'Twitter',
      imageUrl: 'assets/AssetsCanauxImages/twitter.png'
    }

];

//======================= display only checked canals in the canals Page =======================
getChecked(allCanals:any){

  let canals = Object.entries(allCanals)
  .filter(([key, value]) => key !== "idUser" && value === true)
  .map(([key, value]) => key);

  console.log(this.cards.filter(obj=>canals.includes(obj.name.toLowerCase())))

  return  this.cards.filter(obj=>canals.includes(obj.name.toLowerCase()))
  

}
//======================= display only unChecked canals in the canals POPUP =======================
getUnChecked(allCanals:any){

  let canals = Object.entries(allCanals)
  .filter(([key, value]) => key !== "idUser" && value === false)
  .map(([key, value]) => key);
   return  this.cards.filter(obj=>canals.includes(obj.name.toLowerCase()))
  

}

}
