export class User{
    constructor(public id:number,
                public username:string,
                public email:string,
                public firstname:string,
                public lastName:string,
                public company:string,
                public address:string,
                public city:string,
                public country:string,
                public profileimage:any,
                public aboutme:string,
                public zipcode:number
                ){}
}