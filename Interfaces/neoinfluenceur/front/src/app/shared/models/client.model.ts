export class Client {
    id?: any;
    username?: string;
    firstname?: string;
    lastname?: string;
    email?:string;
    company?: string;
    address?: string;
    city?: string;
    country?: string;
    aboutme?: string;
    zipcode?: any;
    profileimage?:any;
}
 