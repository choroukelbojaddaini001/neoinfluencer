import { Canal } from "./canals.model";
import { Product } from "./product.model";
import { User } from "./user.model";

export class Project{
    constructor(public id:number,public product:Product,public chosenCanal:string,public user:User){}
}