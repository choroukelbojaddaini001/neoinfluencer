import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from '../models/client.model';
import { LocalStorageService } from './storage/local-storage.service';
const baseUrl = 'http://localhost:8080/api/v1/users';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }
  private getHeaders(): HttpHeaders {
    const token = LocalStorageService.getToken();
    return new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
  }
//getAllUsers
  getAll(): Observable<Client[]> {
    const headers = this.getHeaders();
    return this.http.get<Client[]>(baseUrl,{ headers });
  }
//get user by Id 
  getbyId(id: any): Observable<Client> {
    const headers = this.getHeaders();
    return this.http.get(`${baseUrl}/${id}`,{ headers });
  }


  updateUser(id: any, data: any): Observable<any> {
    const headers = this.getHeaders();
    return this.http.put(`${baseUrl}/Updateusers/${id}`, data,{ headers });
  }
  //update User profile : 
  updateUserImage(id:any,image:any):Observable<any>{
    const headers = this.getHeaders();
    return this.http.put(`${baseUrl}/upload/image/${id}`, image,{ headers })
  }
  //Delete user By id 
  // delete(id: any): Observable<any> {
  //   return this.http.delete(`${baseUrl}/${id}`);
  // }
//Create a User 
  // create(data: any): Observable<any> {
  //   return this.http.post(baseUrl, data);
  // }

}
