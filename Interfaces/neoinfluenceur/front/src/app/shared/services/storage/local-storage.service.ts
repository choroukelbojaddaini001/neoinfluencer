import { Injectable } from '@angular/core';

const TOKEN = "I_token";
const USERID = "I_user";
const USER = "I_userF";
const USERROLE = "I_role"
@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  saveUserId(userId: any) {
    window.localStorage.removeItem(USERID);
    window.localStorage.setItem(USERID, userId);
  }
  saveUser(user: any) {
    window.localStorage.removeItem(USER);
    window.localStorage.setItem(USER, JSON.stringify(user));
  }

  saveUserRole(role: any) {
    window.localStorage.removeItem(USERROLE);
    window.localStorage.setItem(USERROLE, role);
  }


  saveToken(token: any) {
    window.localStorage.removeItem(TOKEN);
    window.localStorage.setItem(TOKEN, token);
  }

  static getToken(): string |null{
    return localStorage.getItem(TOKEN)
  }

  static hasToken(): boolean {
    if (this.getToken() === null) {
      return false;
    }
    return true;
  }


  static isUserLoggedIn(): boolean {
    if (this.getToken() === null) {
      return false;
    }
    const role: string = this.getUserRole();
    return role == "USER"
  }

  static getUserRole(): string {
    const user = this.getUser();
    if (user == null) {
      return '';
    }
    return user.role;
  }

  static getUser(): any | null {
    const storedValue: any | null = localStorage.getItem(USER);
    console.log(storedValue)
    return storedValue !== null ? JSON.parse(storedValue) : null;
  }
  static getId(): any | null {
    const storedValue: any | null = localStorage.getItem(USERID);
    console.log(storedValue)
    return storedValue !== null ? JSON.parse(storedValue) : null;
  }

  static isAdminLoggedIn(): boolean {
    if (this.getToken() === null) {
      return false;
    }
    const role: string = this.getUserRole();
    return role == "ADMIN"
  }
  static signOut(): void {
    localStorage.removeItem(TOKEN);
    localStorage.removeItem(USERID);
    localStorage.removeItem(USER);
    localStorage.removeItem(USERROLE);
  }
}
