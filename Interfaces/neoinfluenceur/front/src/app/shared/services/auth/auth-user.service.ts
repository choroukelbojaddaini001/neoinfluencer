import { Injectable } from '@angular/core';
import { LocalStorageService } from '../storage/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthUserService {

  userId=0
  isUserLoggedIn : boolean = LocalStorageService.isUserLoggedIn()
  constructor() { }
  getId(){
    if(LocalStorageService.isUserLoggedIn()==true && LocalStorageService.getId()!=null){
       this.userId = LocalStorageService.getId()
       return this.userId

    }else{
      return null
    }
  }
  logout(){
    LocalStorageService.signOut();
    
  }
}
