import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from '../models/project.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectServiceService {

  private baseURL ='http://localhost:8080/projects/search';
  private apigetAllProjectsUrl = 'http://localhost:8080/projects/get/all';
  private addNewProjectUrl = 'http://localhost:8080/projects/add';

  constructor(private http:HttpClient) { }

  fetchProjects(userId:number):Observable<any[]>{
    return this.http.get<any[]>(`${this.apigetAllProjectsUrl}?userId=${userId}`);
  }

  searchProjectByProductName(productName:string,userId:number):Observable<Project[]>{
    console.log("calling the search request",productName)
    const searchUrl = `${this.baseURL}?productName=${productName}&userId=${userId}`;
    console.log("this is the search url",searchUrl)
    return this.http.get<Project[]>(searchUrl);
  }

  addProject(project: any): Observable<Project> {
    return this.http.post<Project>(this.addNewProjectUrl, project);
  }
  

}
