import { Component } from '@angular/core';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css']
})
export class MainLayoutComponent {

  openMenuVal!: boolean;

  openMenu(event :boolean){

    this.openMenuVal = event;
    console.log(this.openMenuVal);
  }

}
