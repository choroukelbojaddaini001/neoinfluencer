import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SuivreTendanceComponent } from './suivre-tendance/suivre-tendance.component';
import { ProfilComponent } from './profil/profil.component';
import {MatDialogModule} from '@angular/material/dialog';
import { RouterModule } from '@angular/router';
import {MatStepperModule} from '@angular/material/stepper';
import { FormsModule } from '@angular/forms';
import { AngularD3CloudModule } from 'angular-d3-cloud'
import { ReactiveFormsModule } from '@angular/forms';
import { PostsComponent } from './posts/posts.component';
import { DatePipe } from '@angular/common';
import {MatCheckboxModule} from '@angular/material/checkbox'
import { CanvasJSAngularChartsModule } from '@canvasjs/angular-charts';
import { NgApexchartsModule } from "ng-apexcharts";
import { NgxSpinnerModule } from 'ngx-spinner';


@NgModule({
  declarations: [
    DashboardComponent,
    SuivreTendanceComponent,
    ProfilComponent,
      PostsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    MatDialogModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    AngularD3CloudModule,
    CanvasJSAngularChartsModule,
    NgApexchartsModule,
    NgxSpinnerModule

  ],
  exports:[
    DashboardComponent,
    SuivreTendanceComponent,
    ProfilComponent,
    PostsComponent
   ], providers: [DatePipe],
})
export class FeaturesModule { }
