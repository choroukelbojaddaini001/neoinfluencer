import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { NavigationExtras, Router } from '@angular/router';
import { CanalService } from 'src/app/shared/services/canal.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-canaux',
  templateUrl: './canaux.component.html',
  styleUrls: ['./canaux.component.css']
})
export class CanauxComponent {

  canals: string[] = [];
  canaux={
    facebook:false,
    youtube:false
  };
    constructor(private dialog:MatDialog,private http: HttpClient, private router:Router,
      private canalService:CanalService
      ){

 
  }
  selectCanal(canal:string){
    //================ uncheck the other canal if one of them get checked =====================
    if(canal=="youtube"){
      this.canaux ={
        facebook:false,
        youtube:true
      }
    this.canals[0]=canal

    }else{
      this.canaux ={
        facebook:true,
        youtube:false
      }
    this.canals[0]=canal


    }
       
    console.log("fb "+this.canaux['facebook']," yt "+this.canaux['youtube'])
     
    console.log(this.canals);

  }

  confirmer(){

    if(this.canals.length>0){
    console.log("fb "+this.canaux['facebook']," yt "+this.canaux['youtube'])
      
      this.canalService.chosenCanal=this.canals[0]
      this.router.navigate(['/TrendingProject'])
    }else{
      Swal.fire({
        icon: 'error',
        title: 'Pay attention',
        text: '🚨You should choose a canal',
        showCancelButton: true,
        confirmButtonText: 'OK',
      })
          // ...
        ; 
    }
  }
}
