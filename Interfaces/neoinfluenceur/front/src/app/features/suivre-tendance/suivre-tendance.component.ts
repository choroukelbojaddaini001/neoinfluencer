import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostsService } from 'src/app/shared/services/posts.service';
import { FlaskApiService } from 'src/app/services/flask-api.service';
import { scaleLinear, scaleOrdinal } from 'd3-scale';
import { schemeCategory10, schemeBlues, schemeGreens, schemePastel1, schemePastel2 } from 'd3-scale-chromatic';
import Chart from 'chart.js/auto';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexYAxis,
  ApexXAxis,
  ApexFill,
  ApexGrid,
  ApexMarkers
} from "ng-apexcharts";
// spinner
import { NgxSpinnerService } from 'ngx-spinner';
// ende import spinner
export type ChartOptiones = {
  series: any;
  chart:any;
  xaxis: any;
  yaxis: any;
  grid: any;
  fill: any;
  colors: any;
  legend: any; //ApexLegend;
  markers: any;
};
@Component({
  selector: 'app-suivre-tendance',
  templateUrl: './suivre-tendance.component.html',
  styleUrls: ['./suivre-tendance.component.css']
})
export class SuivreTendanceComponent  {
  selectedPosts!: any;
  posComments!:any;
  negComments!:any;
  neutComments!:any;
  //Dans le cas de deux postes selection.
  positive!:any;
  negative!:any;
  neutral!:any;
  positiveSec!:any;
  negativeSec!:any;
  neutralSec!:any;
  //======end=====//
  public pourcentagePositif = 0;
  public pourcentageNegatif = 0;
  public pourcentageNeutre = 0;
  pageData!:any;
  nbPost!:any;
  fbPosts!:any;
  public str:any;
  data: any;
  comments!:any;
  resultPredictions:String[]= [];
  commentsFrstPost!:any;
  commentsSecPost!:any;
  predicFrstPost:String[] = [];
  predicSecPost : String[] = [];
  public isExecuted = false;
  public rotate: any = 0;
  public fillMapper: any;
  public fillFx: any;
  public title: string = "Welcome to d3-cloud-angular demo";
  public animations: boolean = true;
	public charte: any;
  public rotateScale:any;
  public autoFill: boolean = true;

  public schemas: any[] = [
    {"id": 0, "name": "Category10", "schema": schemeCategory10 },
    {"id": 1, "name": "Blues", "schema": schemeBlues[9] },
    {"id": 2, "name": "Greens", "schema": schemeGreens[9] },
    {"id": 3, "name": "Pastel1", "schema": schemePastel1 },
    {"id": 4, "name": "Pastel2", "schema": schemePastel2 },
  ];

  public options: any = {
    autoFill: true,
    rotate: true,
    fillScheme: 0,
    animations: true
  };
  // public charte: any;
  public chart: any;
  public Barchart:any;
  public DoughnutChart:any;
  public DoughnutChartFirst:any;
  public chartBar:any;
  public Classchart:any;
  public Classchart2:any;
  public canal:any;
  public colorsBar:any;
  date : any = [];
  commentsPost : any = [];
  likesPost :any = [];
  sharesPost :any = [];
  divAffichee: string = 'un'; 
  youtubeComments: any
  videoDetails:any
  videoUrl:any
  idVideo:any
 public vid!:any
 // start declaration of spinner
 isLoading: boolean = true;
 // end declaration of spinner
  @ViewChild('barCanvas') barCanvas: ElementRef | undefined;
// Scatter
// @ViewChild("chart") charte: ChartComponent | undefined;
public chartOptiones!: any ;
// end Scatter
constructor(private route:ActivatedRoute,private spinner: NgxSpinnerService,private postsService: PostsService,private flaskApiService: FlaskApiService,private sanitizer: DomSanitizer){
  this.canal= history.state.canal;
}
  ngOnInit(){
    if(this.canal!==undefined){
    this.spinner.show();

    }
    
    this.youtubeComments= history.state.youtubeComments;
this.videoDetails =history.state.videoDetails;
this.idVideo=history.state.idVideo;
console.log("thiiiiiis canaaaaaal",this.canal)
    if(this.canal==='youtube'){
      this.colorsBar = '#30c0c0', '#f0f357','#dcdcdc',
      console.log('this is details of video',this.videoDetails);
      console.log('this is comments of video',this.youtubeComments);
      console.log('this is comments of video',this.videoDetails?.id);
      this.videoUrl = "https://www.youtube.com/embed/"+this.videoDetails.id;
      this.vid = this.sanitizer.bypassSecurityTrustResourceUrl(this.videoUrl);

      console.log('this is video url okay ',this.videoUrl);
      this.comments = this.youtubeComments;
      this.str=this.comments.join(' ')
    console.log('this str ', this.str);

    this.predictAllComments(this.comments,this.resultPredictions)

    }
    
    if(this.canal==='facebook')
    {
      this.colorsBar = '#30c0c0', '#f0f357','#dcdcdc',
      this.selectedPosts = history.state.selectedPosts
      console.log('this are sekected posts akhouri ',this.selectedPosts?.length);
      console.log('Voici les commentaires de tous les posts en un seul commentaire :');
      this.comments = this.selectedPosts.reduce((comments :any, post:any) => {
        if (post.comments && post.comments.data) {
          const postComments = post.comments.data.map((comment: { message: any; }) => comment.message);
          return comments.concat(postComments);
        }
        return comments;
      }, []);
      
        this.str=this.comments.join(' ')
      console.log('this str ', this.str);
  
      const comments = this.selectedPosts[0]?.comments?.data || [];
   
  // Extraire les messages des commentaires
   this.commentsFrstPost = comments.map((comment: { message: any; }) => comment.message);
  
  console.log("mssssss",this.commentsFrstPost);
  const comments2 = this.selectedPosts[1]?.comments?.data || [];
  
  // Extraire les messages des commentaires
   this.commentsSecPost = comments2.map((comment: { message: any; }) => comment.message);
  
  // console.log("mssssss2222",this.commentsSecPost);
  
  
      this.pageData = history.state.pageData
      this.nbPost = history.state.nbPost;
      this.fbPosts = history.state.fbPosts;
      // console.log("hahoma homa haado",this.selectedPosts)
      console.log(this.fbPosts[5])
      this.getPostsByDay();
    //   console.log("this are post Data",this.pageData)
    //   console.log("ha chhal dyal posts",this.nbPost)
    //  console.log("hado les commentaires",this.comments)
      this.predictAllComments(this.commentsFrstPost,this.predicFrstPost)
      this.predictAllComments(this.commentsSecPost,this.predicSecPost)
      this.predictAllComments(this.comments,this.resultPredictions)
      
  
    }
      
    // this.predictAllComments();
    this.rotateScale = scaleLinear().range([-90, 90]);
    this.rotateScale.domain([0,1]);
    this.initData();
    this.applyOptions();
    
  }
  async predictSentiment(data: any[]) {
    return new Promise((resolve, reject) => {
      this.flaskApiService.predictSentiment(data).subscribe(response => {
        console.log(data);
        console.log(" : ");
        console.log('The predicted sentiment is', response);
        resolve(response);
      }, error => {
        reject(error);
      });
    });
  }
  
  async predictAllComments(comments: any[], results: any[]) {
    const promises = comments.map(comment => {
      return this.predictSentiment([comment]);
    });
  
    const predictedResults = await Promise.all(promises);
     results.push(...predictedResults);
  
    // this.simulateLoading(); // Simulating loading delay
    this.simulateLoading(); // Simulating loading delay
  }
  classificationComments(){
    this.posComments = this.resultPredictions.filter(x => x=='Positive');
    this.negComments = this.resultPredictions.filter(x => x=='Negative');
    this.neutComments = this.resultPredictions.filter(x => x=='Neutral');
  }
  //=====Dans le cas de deux postes==========//
  classificationCommentsPost1(){
    this.positive = this.predicFrstPost.filter(x => x=='Positive');
    this.negative = this.predicFrstPost.filter(x => x=='Negative');
    this.neutral = this.predicFrstPost.filter(x => x=='Neutral');
  }
  classificationCommentsPost2(){
    this.positiveSec = this.predicSecPost.filter(x => x=='Positive');
    this.negativeSec = this.predicSecPost.filter(x => x=='Negative');
    this.neutralSec = this.predicSecPost.filter(x => x=='Neutral');
  }
  //======END==========//
  ngAfterViewInit() {
  // this.createChart();
    // this.createBarChart();
    
}
ngDoCheck(){
  if(!this.isExecuted && this.resultPredictions?.length == this.comments?.length && this.canal == 'youtube'){
    this.isExecuted = true;
    this.classificationComments();
    this.barChartMethod();
    this.createDoughnutChart();
    const total_comments = this.comments.length
const positive_comments = this.posComments.length
   this.pourcentagePositif = (positive_comments / total_comments) * 100
const neutral_comments = this.neutComments.length
   this.pourcentageNeutre = (neutral_comments / total_comments) * 100
   const negative_comments = this.negComments.length
   this.pourcentageNegatif = (negative_comments / total_comments) * 100
    this.chartOptiones = {
      series: [
        {
          name: "pos",
          data: this.posComments.map((_: any, index: number) => ({
            x: +(Math.random() * (20 - 1) + 1).toFixed(2),
            y: +(Math.random() * (10 - 1) + 1).toFixed(0)
          }))
        },
        {
          name: "neg",
          data: this.negComments.map((_: any, index: number) => ({
            x: +(Math.random() * (30 - 1) + 1).toFixed(2),
            y: +(Math.random() * (10 - 1) + 1).toFixed(0)
          }))
        },
        {
          name: "neut",
          data: this.neutComments.map((_: any, index: number) => ({
            x: +(Math.random() * (40 - 1) + 1).toFixed(2),
            y: +(Math.random() * (10 - 1) + 1).toFixed(0)
          }))
        }
      ],
      chart: {
        height: 250,
        type: "scatter",
        animations: {
          enabled: false
        },
        zoom: {
          enabled: false
        },
        toolbar: {
          show: false
        }
      },
      colors: ["#33cccc", "#ff0000", "#f8a539"],
      xaxis: {
        tickAmount: 10,
        min: 0,
        max: 40,
        labels: {
          rotate: 0,
          trim: false
        }
      },
      yaxis: {
        tickAmount: 7
      },
      markers: {
        size: 5
      },
      fill: {
        type: "solid",
        opacity: 1,
      },
      legend: {
        labels: {
          useSeriesColors: true
        },
        markers: {
          customHTML: [
            function() {
              return '<span> <img src="../../../assets/suivreTendance/pos.png" alt="imgs"/><span>';
            },
            function() {
              return '<span> <img src="../../../assets/suivreTendance/neg.png" alt="imgs"/><span>';
            },
            function() {
              return '<span> <img src="../../../assets/suivreTendance/neut.png" alt="imgs"/><span>';
            }
          ]
        }
      }
    };
    
  }
  if(!this.isExecuted && this.resultPredictions?.length == this.comments?.length && this.selectedPosts?.length > 2 && this.canal == 'facebook'){
    this.isExecuted = true;
    this.classificationComments()
   
    this.createBarChart();
    
    this.barChartMethod();
   this.createDoughnutChart();
   this.chartOptiones = {
    series: [
      {
        name: "pos",
        data: this.posComments.map((_: any, index: number) => ({
          x: +(Math.random() * (20 - 1) + 1).toFixed(2),
          y: +(Math.random() * (10 - 1) + 1).toFixed(0)
        }))
      },
      {
        name: "neg",
        data: this.negComments.map((_: any, index: number) => ({
          x: +(Math.random() * (30 - 1) + 1).toFixed(2),
          y: +(Math.random() * (10 - 1) + 1).toFixed(0)
        }))
      },
      {
        name: "neut",
        data: this.neutComments.map((_: any, index: number) => ({
          x: +(Math.random() * (40 - 1) + 1).toFixed(2),
          y: +(Math.random() * (10 - 1) + 1).toFixed(0)
        }))
      }
    ],
    chart: {
      height: 250,
      type: "scatter",
      animations: {
        enabled: false
      },
      zoom: {
        enabled: false
      },
      toolbar: {
        show: false
      }
    },
    colors: ["#33cccc", "#ff0000", "#f8a539"],
    xaxis: {
      tickAmount: 10,
      min: 0,
      max: 40,
      labels: {
        rotate: 0,
        trim: false
      }
    },
    yaxis: {
      tickAmount: 7
    },
    markers: {
      size: 5
    },
    fill: {
      type: "solid",
      opacity: 1,
    },
    legend: {
      labels: {
        useSeriesColors: true
      },
      markers: {
        customHTML: [
          function() {
            return '<span> <img src="../../../assets/suivreTendance/pos.png" alt="imgs"/><span>';
          },
          function() {
            return '<span> <img src="../../../assets/suivreTendance/neg.png" alt="imgs"/><span>';
          },
          function() {
            return '<span> <img src="../../../assets/suivreTendance/neut.png" alt="imgs"/><span>';
          }
        ]
      }
    }
  };
  //  this.createDoughnutChartSecond();

   
     this.resultPredictions = []
    
  }
  
  if(this.selectedPosts?.length == 2  && this.resultPredictions?.length == this.comments?.length && this.predicFrstPost?.length == this.commentsFrstPost?.length && this.predicSecPost?.length == this.commentsSecPost?.length && this.canal == 'facebook'){
    this.classificationCommentsPost1();
    this.classificationComments()

    this.createDoughnutChartFirst();
     this.classificationCommentsPost2();
    this.createDoughnutChartSec();
    this.barChartTwoPosts();
    this.chartOptiones = {
      series: [
        {
          name: "pos",
          data: this.posComments.map((_: any, index: number) => ({
            x: +(Math.random() * (20 - 1) + 1).toFixed(2),
            y: +(Math.random() * (10 - 1) + 1).toFixed(0)
          }))
        },
        {
          name: "neg",
          data: this.negComments.map((_: any, index: number) => ({
            x: +(Math.random() * (30 - 1) + 1).toFixed(2),
            y: +(Math.random() * (10 - 1) + 1).toFixed(0)
          }))
        },
        {
          name: "neut",
          data: this.neutComments.map((_: any, index: number) => ({
            x: +(Math.random() * (40 - 1) + 1).toFixed(2),
            y: +(Math.random() * (10 - 1) + 1).toFixed(0)
          }))
        }
      ],
      chart: {
        height: 250,
        type: "scatter",
        animations: {
          enabled: false
        },
        zoom: {
          enabled: false
        },
        toolbar: {
          show: false
        }
      },
      colors: ["#33cccc", "#ff0000", "#f8a539"],
      xaxis: {
        tickAmount: 10,
        min: 0,
        max: 40,
        labels: {
          rotate: 0,
          trim: false
        }
      },
      yaxis: {
        tickAmount: 7
      },
      markers: {
        size: 5
      },
      fill: {
        type: "solid",
        opacity: 1,
      },
      legend: {
        labels: {
          useSeriesColors: true
        },
        markers: {
          customHTML: [
            function() {
              return '<span> <img src="../../../assets/suivreTendance/pos.png" alt="imgs"/><span>';
            },
            function() {
              return '<span> <img src="../../../assets/suivreTendance/neg.png" alt="imgs"/><span>';
            },
            function() {
              return '<span> <img src="../../../assets/suivreTendance/neut.png" alt="imgs"/><span>';
            }
          ]
        }
      }
    };
     this.predicFrstPost = []
     this.predicSecPost = []
  }
  if(!this.isExecuted && this.resultPredictions?.length == this.comments?.length && this.selectedPosts?.length == 1 && this.canal == 'facebook'){
    this.isExecuted = true;
    this.classificationComments()
   
    this.createBarChart();
    
    this.barChartMethod();
   this.createDoughnutChart();
   const total_comments = this.comments.length
const positive_comments = this.posComments.length
   this.pourcentagePositif = (positive_comments / total_comments) * 100
const neutral_comments = this.neutComments.length
   this.pourcentageNeutre = (neutral_comments / total_comments) * 100
   const negative_comments = this.negComments.length
   this.pourcentageNegatif = (negative_comments / total_comments) * 100
   this.chartOptiones = {
    series: [
      {
        name: "pos",
        data: this.posComments.map((_: any, index: number) => ({
          x: +(Math.random() * (20 - 1) + 1).toFixed(2),
          y: +(Math.random() * (10 - 1) + 1).toFixed(0)
        }))
      },
      {
        name: "neg",
        data: this.negComments.map((_: any, index: number) => ({
          x: +(Math.random() * (30 - 1) + 1).toFixed(2),
          y: +(Math.random() * (10 - 1) + 1).toFixed(0)
        }))
      },
      {
        name: "neut",
        data: this.neutComments.map((_: any, index: number) => ({
          x: +(Math.random() * (40 - 1) + 1).toFixed(2),
          y: +(Math.random() * (10 - 1) + 1).toFixed(0)
        }))
      }
    ],
    chart: {
      height: 250,
      type: "scatter",
      animations: {
        enabled: false
      },
      zoom: {
        enabled: false
      },
      toolbar: {
        show: false
      }
    },
    colors: ["#33cccc", "#ff0000", "#f8a539"],
    xaxis: {
      tickAmount: 10,
      min: 0,
      max: 40,
      labels: {
        rotate: 0,
        trim: false
      }
    },
    yaxis: {
      tickAmount: 7
    },
    markers: {
      size: 5
    },
    fill: {
      type: "solid",
      opacity: 1,
    },
    legend: {
      labels: {
        useSeriesColors: true
      },
      markers: {
        customHTML: [
          function() {
            return '<span> <img src="../../../assets/suivreTendance/pos.png" alt="imgs"/><span>';
          },
          function() {
            return '<span> <img src="../../../assets/suivreTendance/neg.png" alt="imgs"/><span>';
          },
          function() {
            return '<span> <img src="../../../assets/suivreTendance/neut.png" alt="imgs"/><span>';
          }
        ]
      }
    }
  };
  //  this.createDoughnutChartSecond();

   
     this.resultPredictions = []
    
  }
 
}
  //============================== wordcloud==============
  applyOptions() {
    this.animations = this.options.animations;
    this.autoFill = this.options.autoFill;
    if (this.options.rotate) {
      this.rotate = () => {
        return this.rotateScale(Math.random());
      }
    } else {
      this.rotate = 0;
    }

    this.fillFx = scaleOrdinal(this.schemas[this.options.fillScheme].schema);

    this.fillMapper = (datum: any, index: number) => {
      return this.fillFx(index.toString());
    }
    this.initData();
  }

  initData() {
    this.data = this.str.split(' ').map((d: any) => {
      return { text: d, value: 10 + Math.random() * 90, fill: '0' };
    })
  }

  onWordClick(event: any) {
    console.log(event)
  }

  createBarChart(){

  
    this.Barchart = new Chart("MyBarChart", {
      type: 'bar', //this denotes tha type of chart

      data: {// values on X-Axis
        labels: this.date, 
	       datasets: [
          {
            label: "Comments",
            data: this.commentsPost,
            backgroundColor: '#30c0c0'
          },
          {
            label: "Likes",
            data: this.likesPost,
            backgroundColor: 'rgb(240, 243, 87)'
          },
          {
            label: "Shares",
            data: this.sharesPost,
            backgroundColor: '#f8a539'
          } 
        ]
      },
      options: {
        aspectRatio:2.5
      }
      
    });
  }
  // createChart(){
  
  //   this.chart = new Chart("MyChart", {
  //     type: 'line', //this denotes tha type of chart

  //     data: {// values on X-Axis
  //       labels: ['2022-05-10', '2022-05-11', '2022-05-12','2022-05-13',
	// 							 '2022-05-14', '2022-05-15', '2022-05-16','2022-05-17', ], 
	//        datasets: [
  //         {
  //           label: "Sales",
  //           data: ['467','576', '572', '79', '92',
	// 							 '574', '573', '576'],
  //           backgroundColor: 'blue'
  //         },
  //         {
  //           label: "Profit",
  //           data: ['542', '542', '536', '327', '17',
	// 								 '0.00', '538', '541'],
  //           backgroundColor: 'limegreen'
  //         }  
  //       ]
  //     },
  //     options: {
  //       aspectRatio:2.5
  //     }
      
  //   });
  // }
  createDoughnutChart(){
    if(this.canal==='facebook'){
        this.DoughnutChart = new Chart("MyDoughnutChart", {
          type: 'doughnut', //this denotes tha type of chart
    
          data: {// values on X-Axis
            labels: [ 'Neg','Pos','Neut', ],
             datasets: [{
        label: 'My First Dataset',
        data: [this.negComments.length, this.posComments.length,this.neutComments.length],
        backgroundColor: [
          '#f8a539',
          '#f0f357',
          '#dcdcdc',			
        ],
        hoverOffset: 4
      }],
          },
          options: {
            aspectRatio:2.5
          }
    
        });
      }
      else{
        this.DoughnutChart = new Chart("MyDoughnutChart", {
          type: 'doughnut', //this denotes tha type of chart
    
          data: {// values on X-Axis
            labels: [ 'Neg','Pos','Neut', ],
             datasets: [{
        label: 'My First Dataset',
        data: [this.negComments.length, this.posComments.length,this.neutComments.length],
        backgroundColor: [
          '#ff0000',
          '#30c0c0',
         '#FFC10A',		
        ],
        hoverOffset: 4
      }],
          },
          options: {
            aspectRatio:2.5
          }
    
        });
      
      }
      }
    
      //======================================= Bar Chart ===========================
      barChartMethod() {
      if(this.canal==='facebook')
    {
        const chartElement = document.getElementById('PosNegChart') as HTMLCanvasElement | null;
      if (chartElement) {
        this.Classchart = new Chart(chartElement, {
          type: 'bar',
          data: {
            labels: ['Neg', 'Pos', 'Neut'],
            datasets: [
              {
                // label: '# of Votes',
                data: [this.negComments.length, this.posComments.length, this.neutComments.length],
                backgroundColor: [
                  
                  '#30c0c0',
                  '#f0f357',
                  '#dcdcdc',
                ],
                borderColor: [
                  '#30c0c0',
                  '#f0f357',
                  '#dcdcdc',
                ],
                borderWidth: 1,
              },
            ],
          },
          options: {
            scales: {
              y: {
                beginAtZero: true,
              },
            },
          },
        });
      }
    }
    else{
      const chartElement = document.getElementById('PosNegChart') as HTMLCanvasElement | null;
      if (chartElement) {
        this.Classchart = new Chart(chartElement, {
          type: 'bar',
          data: {
            labels: ['Neg', 'Pos', 'Neut'],
            datasets: [
              {
                // label: '# of Votes',
                data: [this.negComments.length, this.posComments.length, this.neutComments.length],
                backgroundColor: [
                  
                  '#ff0000',
                  '#30c0c0',
                  '#FFC10A',
                ],
                borderColor: [
                  '#ff0000',
                  '#30c0c0', 
                  '#FFC10A',
                ],
                borderWidth: 1,
              },
            ],
          },
          options: {
            scales: {
              y: {
                beginAtZero: true,
              },
            },
          },
        });
      }
    }
      }

  createDoughnutChartSecond(){
     
       if(this.canal==='facebook'){
        this.DoughnutChartFirst = new Chart("MyDoughnutChartFirst", {
          type: 'doughnut', //this denotes tha type of chart
    
          data: {// values on X-Axis
            labels: [ 'Neg','Pos','Neut', ],
             datasets: [{
        label: 'My First Dataset',
        data: [this.negComments.length, this.posComments.length,this.neutComments.length],
        backgroundColor: [
          '#f8a539',
          '#f0f357',
          '#dcdcdc',			
        ],
        hoverOffset: 4
      }],
          },
          options: {
            aspectRatio:2.5
          }
    
        });
      }
      else{
        this.DoughnutChartFirst = new Chart("MyDoughnutChartFirst", {
          type: 'doughnut', //this denotes tha type of chart
    
          data: {// values on X-Axis
            labels: [ 'Neg','Pos','Neut', ],
             datasets: [{
        label: 'My First Dataset',
        data: [this.negComments.length, this.posComments.length,this.neutComments.length],
        backgroundColor: [
          '#ff0000',
          '#30c0c0',
         '#FFC10A',		
        ],
        hoverOffset: 4
      }],
          },
          options: {
            aspectRatio:2.5
          }
    
        });
      
      }
   
  }


  

   
  
    getPostsByDay(){
      for(let i=0;i<this.fbPosts.length;i++){
     let postDate = this.fbPosts[i].date.split("T")[0]
     let comments,likes,shares
    if(this.fbPosts[i].comments==undefined){
       comments = 0;
    }else{
      comments  = this.fbPosts[i].comments.data.length
    }
    if(this.fbPosts[i].likes==undefined){
      likes = 0;
   }else{
     likes  = this.fbPosts[i].likes.data.length
   }
   if(this.fbPosts[i].shares==undefined){
     
    shares = 0;
 }else{
   shares  = this.fbPosts[i].shares.count
 }

    this.date.unshift(postDate)
    this.commentsPost.unshift(comments)
    this.likesPost.unshift(likes)
    this.sharesPost.unshift(shares)
      }
    }
    afficherDiv(nomDiv: string): void {
      this.divAffichee = nomDiv;
    }
    goToForm(){
      this.divAffichee = 'quatre';
    }


    //==================FOR TWO POSTS=====================//
    createDoughnutChartFirst(){
        
      if(this.canal==='facebook'){
        this.DoughnutChartFirst = new Chart("MyDoughnutChartFirst", {
          type: 'doughnut', //this denotes tha type of chart
    
          data: {// values on X-Axis
            labels: [ 'Neg','Pos','Neut', ],
             datasets: [{
        label: 'My First Dataset',
        data: [this.negative.length, this.positive.length,this.neutral.length],
        backgroundColor: [
          '#f8a539',
          '#f0f357',
          '#dcdcdc',			
        ],
        hoverOffset: 4
      }],
          },
          options: {
            aspectRatio:2.5
          }
    
        });
      }
      else{
        this.DoughnutChartFirst = new Chart("MyDoughnutChartFirst", {
          type: 'doughnut', //this denotes tha type of chart
    
          data: {// values on X-Axis
            labels: [ 'Neg','Pos','Neut', ],
             datasets: [{
        label: 'My First Dataset',
        data: [this.negComments.length, this.posComments.length,this.neutComments.length],
        backgroundColor: [
          '#ff0000',
          '#30c0c0',
         '#FFC10A',		
        ],
        hoverOffset: 4
      }],
          },
          options: {
            aspectRatio:2.5
          }
    
        });
      
      }
   
      
    }
    createDoughnutChartSec(){
        
      if(this.canal==='facebook'){
        this.DoughnutChart = new Chart("MyDoughnutChart", {
          type: 'doughnut', //this denotes tha type of chart
    
          data: {// values on X-Axis
            labels: [ 'Neg','Pos','Neut', ],
             datasets: [{
        label: 'My First Dataset',
        data: [this.negativeSec.length, this.positiveSec.length,this.neutralSec.length],
        backgroundColor: [
          '#f8a539',
          '#f0f357',
          '#dcdcdc',			
        ],
        hoverOffset: 4
      }],
          },
          options: {
            aspectRatio:2.5
          }
    
        });
      }
      else{
        this.DoughnutChart = new Chart("MyDoughnutChart", {
          type: 'doughnut', //this denotes tha type of chart
    
          data: {// values on X-Axis
            labels: [ 'Neg','Pos','Neut', ],
             datasets: [{
        label: 'My First Dataset',
        data: [this.negComments.length, this.posComments.length,this.neutComments.length],
        backgroundColor: [
          '#ff0000',
          '#30c0c0',
         '#FFC10A',		
        ],
        hoverOffset: 4
      }],
          },
          options: {
            aspectRatio:2.5
          }
    
        });
      
      }
   
        
     
    }
    

    barChartTwoPosts() {
      if(this.canal==='facebook'){
      const chartElement = document.getElementById('PosNegChart2') as HTMLCanvasElement;
      if (chartElement) {
        this.Classchart2 = new Chart(chartElement, {
          type: 'bar',
          data: {
            labels: ['Neg', 'Pos', 'Neut'],
            datasets: [
              {
                label: 'Value 1', // Label for the first value
                data: [this.negative.length, this.positive.length, this.neutral.length],
                backgroundColor: '#33cccc',
                borderColor: '#33cccc',
                borderWidth: 1,
              },
              {
                label: 'Value 2', // Label for the second value
                data: [this.negativeSec.length,this.positiveSec.length,this.neutralSec.length],
                backgroundColor: '#f0f357',
                borderColor: '#f0f357',
                borderWidth: 1,
              },
            ],
          },
          options: {
            scales: {
              y: {
                beginAtZero: true,
              },
            },
          },
        });
      }}
     else{
      const chartElement = document.getElementById('PosNegChart2') as HTMLCanvasElement;
      if (chartElement) {
        this.Classchart2 = new Chart(chartElement, {
          type: 'bar',
          data: {
            labels: ['Neg', 'Pos', 'Neut'],
            datasets: [
              {
                label: 'Value 1', // Label for the first value
                data: [this.negative.length, this.positive.length, this.neutral.length],
                backgroundColor: '#ff0000',
                borderColor: '#ff0000',
                borderWidth: 1,
              },
              {
                label: 'Value 2', // Label for the second value
                data: [this.negativeSec.length,this.positiveSec.length,this.neutralSec.length],
                backgroundColor: '#30c0c0',
                borderColor: '#30c0c0',
                borderWidth: 1,
              },
            ],
          },
          options: {
            scales: {
              y: {
                beginAtZero: true,
              },
            },
          },
        });
      }
     }
    }
    
// function of spinner
simulateLoading(): void {
  setTimeout(() => {
    this.isLoading = false;
    this.spinner.hide();
  }, 2000); // Replace 2000 with the desired loading duration in milliseconds
}
// end function of spinner
}