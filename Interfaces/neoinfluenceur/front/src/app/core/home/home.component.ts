import { AnimationStyleMetadata } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { AuthUserService } from 'src/app/shared/services/auth/auth-user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

  constructor(private userAuth: AuthUserService) {}
  userId:any
  customers:any
  packes :any
  ngOnInit(){

    this.userId = this.userAuth.getId()
    this.customers = [
      "../../assets/Designs/l1.svg",
      "../../assets/Designs/l2.png",
      "../../assets/Designs/l3.png",
      "../../assets/Designs/l4.png",
      "../../assets/Designs/l5.png",
      "../../assets/Designs/l6.png",
    ]
    this.packes = [
      {
        type:"Free",
        price:"0 ",
        features:['one channel','500 comment'],
        icon: "../../assets/Designs/1.png"
      },
      {
        type:"Standard",
        price:"200 ",
        features:['two channels','1000 comment'],
        icon: "../../assets/Designs/2.png"

      },
      {
        type:"Premium",
        price:"500 ",
        features:['all channels','unlimetted comments'],
        icon: "../../assets/Designs/3.png"
      }
    ]
  }


}
