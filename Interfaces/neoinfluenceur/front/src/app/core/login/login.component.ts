import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthUserService } from 'src/app/shared/services/auth/auth-user.service';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { ConfirmedValidator } from 'src/app/shared/services/auth/confirmed.validator';
import { LocalStorageService } from 'src/app/shared/services/storage/local-storage.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  
  loginForm:FormGroup = new FormGroup({
    
    username : new FormControl(''),
    password : new FormControl(''),

  })
  submitted = false;
  hide = true;
  isLoading = true;
  constructor(private formBuilder: FormBuilder, private authService:AuthService,  private router:Router, private userAuth: AuthUserService){

  }
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      

      username : ['',[Validators.required, Validators.email]],
    
      password : ['', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(40)
      ]],

    })
    
  }
  get f(){
    return this.loginForm.controls;
  }
  //============================ login ==============================
  login() {

    this.submitted = true;

    if (this.loginForm.invalid) {
      Swal.fire({
        title: 'Login',
        text: 'Fill all the fields',
        icon: 'error',
      });
      return;
    }
   let user = this.loginForm.value
    
   console.log({username:user.username, password:user.password})
   this.authService.login(user.username, user.password).subscribe(
    (res:any) => {
      console.log(res);
     
      Swal.fire({
        title: 'Login',
        text: 'You signed up successfully',
        icon: 'success',
      });
      // if(res?.id!=1){
      //    this.userAuth.userId = res.id
      //    console.log(res.id)
      // }
      // this.onReset()
      if(LocalStorageService.isUserLoggedIn()){
        this.router.navigate(['/dashboard']);
// console.log("yes")
      }
    },
    (error:any) => {
      console.log(error);
      Swal.fire({
        title: 'Login',
        text: 'Login Failed',
        icon: 'error',
      });
    }
  );
  
}
//====================
onReset(): void {
 
  this.submitted = false;

  this.loginForm.reset();
}
}
