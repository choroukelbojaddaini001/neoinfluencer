import os

from flask import Flask, request, jsonify
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.preprocessing.text import Tokenizer
import tensorflow as tf
import numpy as np
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

max_words = 5000
max_len = 50

# Load the model
model = tf.keras.models.load_model('C:/Users/Lenovo/Downloads/NeoInfluenceur/neoinfluencer/Model/Final_model.h5')
import pickle
from keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences

tokenizer_path = os.path.abspath('C:/Users/Lenovo/Downloads/NeoInfluenceur/neoinfluencer/Model/tokenizer.pickle')
ar_stopwords_path = os.path.abspath('C:/Users/Lenovo/Downloads/NeoInfluenceur/neoinfluencer/Model/ar_stopwords.pickle')

# Load the tokenizer
with open(tokenizer_path, 'rb') as handle:
    tokenizer = pickle.load(handle)
# Load the data_cleaning

# Load the ar_stopwords
with open(ar_stopwords_path, 'rb') as ld:
    ar_stopwords = pickle.load(ld)

import string, emoji, re
import pyarabic.araby as ar
import functools, operator
import logging

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)


def get_emoji_regexp():
    # Sort emoji by length to make sure multi-character emojis are matched first
    emojis = sorted(emoji.EMOJI_DATA, key=len, reverse=True)
    pattern = u'(' + u'|'.join(re.escape(u) for u in emojis) + u')'
    return re.compile(pattern)


def data_cleaning(text):
    text = re.sub(r'^https?:\/\/.*[\r\n]*', '', text, flags=re.MULTILINE)
    text = re.sub(r'^http?:\/\/.*[\r\n]*', '', text, flags=re.MULTILINE)
    text = re.sub(r"http\S+", "", text)
    text = re.sub(r"https\S+", "", text)
    text = re.sub(r'\s+', ' ', text)
    # remove non-Arabic characters
    text = re.sub(
        '[^\u0600-\u06FF\u0750-\u077F\u08A0-\u08FF\uFB50-\uFBC1\uFBD3-\uFD3D\uFD50-\uFD8F\uFD92-\uFDC7\uFDF0-\uFDFB\ufe70-\ufefc\s]+',
        ' ', text)
    text = re.sub("(\s\d+)", "", text)
    text = re.sub(r"$\d+\W+|\b\d+\b|\W+\d+$", "", text)
    text = re.sub("\d+", " ", text)
    text = ar.strip_tashkeel(text)
    text = ar.strip_tatweel(text)
    text = text.replace("#", " ");
    text = text.replace("@", " ");
    text = text.replace("_", " ");

    # Remove arabic signs
    text = text[0:2] + ''.join(
        [text[i] for i in range(2, len(text)) if text[i] != text[i - 1] or text[i] != text[i - 2]])
    text = re.sub(r'([@A-Za-z0-9_ـــــــــــــ]+)|[^\w\s]|#|http\S+', '', text)
    from nltk.stem.isri import ISRIStemmer
    text = ISRIStemmer().stem(text)

    translator = str.maketrans('', '', string.punctuation)
    text = text.translate(translator)
    em = text
    em_split_emoji = get_emoji_regexp().split(em)
    em_split_whitespace = [substr.split() for substr in em_split_emoji]
    em_split = functools.reduce(operator.concat, em_split_whitespace)
    text = " ".join(em_split)
    text = re.sub(r'(.)\1+', r'\1', text)
    text = re.sub("[إأآا]", "ا", text)
    text = re.sub("ى", "ي", text)
    text = re.sub("ة", "ه", text)
    text = re.sub("گ", "ك", text)
    text = text.replace("آ", "ا")
    text = text.replace("إ", "ا")
    text = text.replace("أ", "ا")
    text = text.replace("ؤ", "و")
    text = text.replace("ئ", "ي")
    return text



@app.route('/predict', methods=['POST'])
def predict():
    data = request.json.get('data')
    sentiment_classes = ['Negative', 'Neutral', 'Positive']
    max_len = 50
    data = str(data)

    # Preprocess the data using the data_cleaning function
    preprocessed_data = data_cleaning(data)

    # Remove stopwords from the preprocessed data
    preprocessed_data = [word for word in preprocessed_data.split() if word not in ar_stopwords]
    preprocessed_data = ' '.join(preprocessed_data)

    # Transforms preprocessed text to a sequence of integers using a tokenizer object
    xt = tokenizer.texts_to_sequences([preprocessed_data])
    print(xt)
    # Pad sequences to the same length
    xt = pad_sequences(xt, padding='post', maxlen=max_len)
    # Do the prediction using the loaded model
    yt = model.predict(xt).argmax(axis=1)
    # Print the predicted sentiment
    print('The predicted sentiment is', sentiment_classes[yt[0]])

    return jsonify(sentiment_classes[yt[0]])


if __name__ == '__main__':
    app.run(port=3000)
