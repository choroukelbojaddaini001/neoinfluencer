from flask import Flask, request, jsonify
import tensorflow as tf
from transformers import TFAutoModelForSequenceClassification, AutoTokenizer

app = Flask(__name__)

model_path = "C:/Users/hassa/OneDrive/Bureau/Final_model_bert.h5"
tokenizer_path = "tokenizer_path"  # Replace with the actual path to your tokenizer

model = TFAutoModelForSequenceClassification.from_pretrained(model_path)
tokenizer = AutoTokenizer.from_pretrained(tokenizer_path)


@app.route('/predict', methods=['POST'])
def predict():
    data = request.get_json()
    input_text = data['text']

    encoded_input = tokenizer(input_text, padding=True, truncation=True, return_tensors='tf')
    output = model(encoded_input)
    scores = tf.nn.softmax(output.logits, axis=1)

    predicted_class = tf.argmax(scores, axis=1).numpy()[0]
    predicted_label = model.config.id2label[predicted_class]

    result = {'label': predicted_label}
    return jsonify(result)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000)
